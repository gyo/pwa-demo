module.exports = {
  root: true,
  extends: 'eslint:recommended',
  env: {
    browser: true,
    es6: true,
    commonjs: true,
  },
  parserOptions: {
    ecmaVersion: 2017,
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
    },
    sourceType: 'module',
  },
}
